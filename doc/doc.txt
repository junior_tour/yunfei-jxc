1 安装applet-common-1.3-SNAPSHOT.jar到本地仓库

    a) 把applet-common-1.3-SNAPSHOT.jar拷贝到C盘
    b) 命令窗口执行：mvn install:install-file -Dfile=C:/applet-common-1.3-SNAPSHOT.jar -DgroupId=com.applet -DartifactId=applet-common -Dversion=1.3-SNAPSHOT -Dpackaging=jar
    c) 把applet-common-1.3-SNAPSHOT.pom拷贝到 “本地maven仓库路径/com/applet/applet-common/1.3-SNAPSHOT” 路径中

2 开源版使用技术：SpringMVC + JDBCTemplate + Redis + Maven + Postgres + JDK1.8

3 开源版仅供学习，不能商用，功能不全

4 商业完整版（移动端 + PC管理端）：

    进销存微商版==>单商户安装版800、SaaS安装版2000、最新完整源码版(含移动端、PC管理端)3500

    进销存企业版==>单商户安装版1500、SaaS安装版3000、最新完整源码版(含移动端、PC管理端)6000

    企业版针对企业用户，更加专业；
    微商版针对普通人群，力求简单易用，一看就懂

    安装版：购买后 无使用期限限制，升级到最新版200元一次，帮助安装并运行成功；

    源码版：购买后，无使用限制，升级到最新版500元一次，帮助在你方开发电脑上运行成功；

    购买后发现BUG，免费修复

    注意：演示环境不开放用户权限这块内容

5 云飞进销存企业版：

  1）支持销售单、销售出库单、销售退货单分离
  2）支持采购单、采购入库单、采购退货单分离
  3）支持其他采购单、其他销售单
  4）支持开启或者关闭单据审核功能
  5）支持审核流程自定义
  6）支持不同仓库之间调拨商品
  7）企业账户管理
  8）支持收款单、付款单、资金转账单管理
  9）支持其他收入单、其他支出单管理
  10）支持商品多规格、按照客户或批发数量设置销售价、按照供应商或者批发数量设置进货价
  11）支持开启或关闭负库存
  12）支持开启或者关闭非编辑状态下的单据备注也能修改
  13）支持开启或者关闭任意状态下可上传附件
  14）支持开启或者关闭单据税率
  15）支持自定义打印单据样式
  16）支持主要模块设置自定义字段
  17）支持自定义单据编号规则
  18）移动端支持H5、小程序、APP
  19）支持替换公司的信息包括：logo、系统名称、移动端菜单图标
  20）私有化部署

  体验环境：
  	体验地址：http://qyjxc.yunfeisoft.com
  	体验账密：test/123456a

  Android端下载地址：http://qyjxc.yunfeisoft.com/updown/app/jxce.2.0.apk
  （其他移动端暂无体验环境）