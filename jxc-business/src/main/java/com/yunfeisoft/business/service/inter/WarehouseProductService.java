package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.applet.utils.Page;
import com.yunfeisoft.business.model.WarehouseProduct;

import java.util.List;
import java.util.Map;

/**
 * ClassName: WarehouseProductService
 * Description: 仓库商品信息service接口
 * Author: Jackie liu
 * Date: 2020-08-04
 */
public interface WarehouseProductService extends BaseService<WarehouseProduct, String> {

    public Page<WarehouseProduct> queryPage(Map<String, Object> params);

    public List<WarehouseProduct> queryList(Map<String, Object> params);

    public WarehouseProduct queryStockSum(String productId);
}