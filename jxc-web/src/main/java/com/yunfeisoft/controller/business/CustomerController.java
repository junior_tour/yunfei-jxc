package com.yunfeisoft.controller.business;

import com.alibaba.excel.EasyExcel;
import com.applet.base.BaseController;
import com.applet.utils.*;
import com.yunfeisoft.business.model.Customer;
import com.yunfeisoft.business.service.inter.CodeBuilderService;
import com.yunfeisoft.business.service.inter.CustomerService;
import com.yunfeisoft.excel.listener.CustomerExcelListener;
import com.yunfeisoft.model.User;
import com.yunfeisoft.utils.ApiUtils;
import com.yunfeisoft.utils.PinyinUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.*;

/**
 * ClassName: CustomerController
 * Description: 客户信息Controller
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Controller
public class CustomerController extends BaseController {

    @Autowired
    private CustomerService customerService;
    @Autowired
    private CodeBuilderService codeBuilderService;

    /**
     * 添加客户信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/customer/save", method = RequestMethod.POST)
    @ResponseBody
    public Response save(Customer record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "name", "名称为空");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }

        User user = ApiUtils.getLoginUser();
        record.setOrgId(user.getOrgId());

        boolean dupName = customerService.isDupName(user.getOrgId(), record.getId(), record.getName());
        if (dupName) {
            return ResponseUtils.warn("该名称已经存在");
        }

        String code = codeBuilderService.generateCode("CUSTOMER", 4, user.getOrgId());
        record.setCode(code);
        record.setBalance(BigDecimal.ZERO);

        if (StringUtils.isBlank(record.getPinyinCode())) {
            record.setPinyinCode(PinyinUtils.getPinYinHeadChar(record.getName()));
        } else {
            record.setPinyinCode(record.getPinyinCode().toUpperCase());
        }

        customerService.save(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 修改客户信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/customer/modify", method = RequestMethod.POST)
    @ResponseBody
    public Response modify(Customer record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "id", "参数错误");
        validator.required(request, "name", "名称为空");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }

        User user = ApiUtils.getLoginUser();
        boolean dupName = customerService.isDupName(user.getOrgId(), record.getId(), record.getName());
        if (dupName) {
            return ResponseUtils.warn("该名称已经存在");
        }

        if (StringUtils.isBlank(record.getPinyinCode())) {
            record.setPinyinCode(PinyinUtils.getPinYinHeadChar(record.getName()));
        } else {
            record.setPinyinCode(record.getPinyinCode().toUpperCase());
        }

        customerService.modify(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 查询客户信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/customer/query", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response query(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        Customer record = customerService.load(id);
        return ResponseUtils.success(record);
    }

    /**
     * 分页查询客户信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/customer/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        String name = ServletRequestUtils.getStringParameter(request, "name", null);

        User user = ApiUtils.getLoginUser();

        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        params.put("name", name);
        params.put("orgId", user.getOrgId());

        Page<Customer> page = customerService.queryPage(params);
        return ResponseUtils.success(page);
    }

    /**
     * 批量删除客户信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/customer/delete", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response delete(HttpServletRequest request, HttpServletResponse response) {
        String ids = ServletRequestUtils.getStringParameter(request, "ids", null);
        if (StringUtils.isBlank(ids)) {
            return ResponseUtils.warn("参数错误");
        }
        customerService.remove(ids.split(","));
        return ResponseUtils.success("删除成功");
    }

    /**
     * 导出excel
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/customer/exportExcel", method = {RequestMethod.POST, RequestMethod.GET})
    public void exportExcel(HttpServletRequest request, HttpServletResponse response) {
        String name = ServletRequestUtils.getStringParameter(request, "name", null);
        User user = ApiUtils.getLoginUser();

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("name", name);
        params.put("orgId", user.getOrgId());

        List<Customer> list = customerService.queryList(params);
        try {
            response.setContentType("application/vnd.ms-excel");
            response.setCharacterEncoding("utf-8");
            // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
            String fileName = URLEncoder.encode("客户信息_" + DateUtils.getNowTime(), "UTF-8");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");

            // 根据用户传入字段 假设我们要忽略 date
            Set<String> excludeColumnFiledNames = new HashSet<String>();
            excludeColumnFiledNames.add("id");
            excludeColumnFiledNames.add("orgId");
            excludeColumnFiledNames.add("type");
            excludeColumnFiledNames.add("isDel");
            excludeColumnFiledNames.add("exists");
            excludeColumnFiledNames.add("createTime");
            excludeColumnFiledNames.add("modifyTime");
            excludeColumnFiledNames.add("createId");
            excludeColumnFiledNames.add("modifyId");

            // 这里需要设置不关闭流
            EasyExcel.write(response.getOutputStream(), Customer.class)
                    .excludeColumnFiledNames(excludeColumnFiledNames)
                    .autoCloseStream(Boolean.FALSE)
                    .sheet("客户信息")
                    .doWrite(list);

        } catch (Exception e) {
            e.printStackTrace();
            // 重置response
            response.reset();
            //response.setContentType("application/json");
            //response.setCharacterEncoding("utf-8");
            AjaxUtils.ajaxJsonWarnMessage("下载失败");
        }
    }

    /**
     * 导入excel
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/customer/importExcel", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response importExcel(@RequestParam("file") MultipartFile file, HttpServletRequest request, HttpServletResponse response) {
        try {
            User user = ApiUtils.getLoginUser();
            EasyExcel.read(file.getInputStream(), Customer.class, new CustomerExcelListener(user.getOrgId(), user.getId())).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseUtils.success("解析导入文件异常");
        }
        return ResponseUtils.success("导入成功");
    }
}
