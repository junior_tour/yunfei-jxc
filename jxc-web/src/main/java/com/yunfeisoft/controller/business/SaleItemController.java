package com.yunfeisoft.controller.business;

import com.applet.base.BaseController;
import com.applet.utils.Page;
import com.applet.utils.Response;
import com.applet.utils.ResponseUtils;
import com.yunfeisoft.business.model.SaleItem;
import com.yunfeisoft.business.model.SaleOrder;
import com.yunfeisoft.business.service.inter.SaleItemService;
import com.yunfeisoft.model.User;
import com.yunfeisoft.utils.ApiUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: SaleItemController
 * Description: 销售单商品信息Controller
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Controller
public class SaleItemController extends BaseController {

    @Autowired
    private SaleItemService saleItemService;

    /**
     * 分页查询销售单商品信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/saleItem/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        String productName = ServletRequestUtils.getStringParameter(request, "productName", null);
        String customerName = ServletRequestUtils.getStringParameter(request, "customerName", null);
        String beginDate = ServletRequestUtils.getStringParameter(request, "beginDate", null);
        String endDate = ServletRequestUtils.getStringParameter(request, "endDate", null);
        String createName = ServletRequestUtils.getStringParameter(request, "createName", null);
        String productId = ServletRequestUtils.getStringParameter(request, "productId", null);

        if (StringUtils.isNotBlank(beginDate)) {
            beginDate += " 00:00:00";
        }

        if (StringUtils.isNotBlank(endDate)) {
            endDate += " 23:59:59";
        }

        User user = ApiUtils.getLoginUser();

        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        params.put("orgId", user.getOrgId());
        params.put("productName", productName);
        params.put("beginDate", beginDate);
        params.put("endDate", endDate);
        params.put("createName", createName);
        params.put("customerName", customerName);
        params.put("productId", productId);
        params.put("orderStatus", SaleOrder.SaleOrderStatusEnum.DELIVERED.getValue());

        Page<SaleItem> page = saleItemService.queryPage(params);
        return ResponseUtils.success(page);
    }

}
