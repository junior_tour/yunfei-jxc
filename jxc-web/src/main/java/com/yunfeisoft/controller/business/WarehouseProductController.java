package com.yunfeisoft.controller.business;

import com.alibaba.excel.EasyExcel;
import com.applet.base.BaseController;
import com.applet.utils.*;
import com.yunfeisoft.business.model.WarehouseProduct;
import com.yunfeisoft.business.service.inter.WarehouseProductService;
import com.yunfeisoft.model.User;
import com.yunfeisoft.utils.ApiUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.*;

/**
 * ClassName: WarehouseProductController
 * Description: 仓库商品信息Controller
 * Author: Jackie liu
 * Date: 2020-08-04
 */
@Controller
public class WarehouseProductController extends BaseController {

    @Autowired
    private WarehouseProductService warehouseProductService;

    /**
     * 添加仓库商品信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    /*@RequestMapping(value = "/web/warehouseProduct/save", method = RequestMethod.POST)
    @ResponseBody
    public Response save(WarehouseProduct record, HttpServletRequest request, HttpServletResponse response) {
        warehouseProductService.save(record);
        return ResponseUtils.success("保存成功");
    }*/

    /**
     * 修改仓库商品信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    /*@RequestMapping(value = "/web/warehouseProduct/modify", method = RequestMethod.POST)
    @ResponseBody
    public Response modify(WarehouseProduct record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "id", "参数错误");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }
        warehouseProductService.modify(record);
        return ResponseUtils.success("保存成功");
    }*/

    /**
     * 查询仓库商品信息
     *
     * @param request
     * @param response
     * @return
     */
    /*@RequestMapping(value = "/web/warehouseProduct/query", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response query(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        WarehouseProduct record = warehouseProductService.load(id);
        return ResponseUtils.success(record);
    }*/

    /**
     * 分页查询仓库商品信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/warehouseProduct/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        String warehouseId = ServletRequestUtils.getStringParameter(request, "warehouseId", null);
        String productName = ServletRequestUtils.getStringParameter(request, "productName", null);
        String productCode = ServletRequestUtils.getStringParameter(request, "productCode", null);
        String warnType = ServletRequestUtils.getStringParameter(request, "warnType", null);
        String pinyinCode = ServletRequestUtils.getStringParameter(request, "pinyinCode", null);
        String namePinyin = ServletRequestUtils.getStringParameter(request, "namePinyin", null);

        User user = ApiUtils.getLoginUser();

        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        params.put("orgId", user.getOrgId());
        params.put("warehouseId", warehouseId);
        params.put("productName", productName);
        params.put("productCode", productCode);
        params.put("warnType", warnType);
        params.put("pinyinCode", StringUtils.upperCase(pinyinCode));
        params.put("namePinyin", namePinyin);

        Page<WarehouseProduct> page = warehouseProductService.queryPage(params);
        return ResponseUtils.success(page);
    }

    /**
     * 分页查询仓库商品信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/warehouseProduct/share/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response shareList(HttpServletRequest request, HttpServletResponse response) {
        String warehouseId = ServletRequestUtils.getStringParameter(request, "warehouseId", null);
        String productName = ServletRequestUtils.getStringParameter(request, "productName", null);
        String warnType = ServletRequestUtils.getStringParameter(request, "warnType", null);
        String orgId = ServletRequestUtils.getStringParameter(request, "orgId", "_no_");

        if (StringUtils.isBlank(orgId)) {
            orgId = "_no_";
        }
        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        params.put("orgId", orgId);
        params.put("warehouseId", warehouseId);
        params.put("productName", productName);
        params.put("warnType", warnType);

        Page<WarehouseProduct> page = warehouseProductService.queryPage(params);
        return ResponseUtils.success(page);
    }

    /**
     * 查询仓库商品列表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/warehouseProduct/queryList", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response queryList(HttpServletRequest request, HttpServletResponse response) {
        String warehouseId = ServletRequestUtils.getStringParameter(request, "warehouseId", null);
        String productId = ServletRequestUtils.getStringParameter(request, "productId", null);
        String productName = ServletRequestUtils.getStringParameter(request, "productName", null);
        String productCode = ServletRequestUtils.getStringParameter(request, "productCode", null);
        String warnType = ServletRequestUtils.getStringParameter(request, "warnType", null);

        User user = ApiUtils.getLoginUser();

        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        params.put("orgId", user.getOrgId());
        params.put("productId", productId);
        params.put("warehouseId", warehouseId);
        params.put("productName", productName);
        params.put("productCode", productCode);
        params.put("warnType", warnType);

        List<WarehouseProduct> list = warehouseProductService.queryList(params);
        return ResponseUtils.success(list);
    }

    /**
     * 批量删除仓库商品信息
     *
     * @param request
     * @param response
     * @return
     */
    /*@RequestMapping(value = "/web/warehouseProduct/delete", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response delete(HttpServletRequest request, HttpServletResponse response) {
        String ids = ServletRequestUtils.getStringParameter(request, "ids", null);
        if (StringUtils.isBlank(ids)) {
            return ResponseUtils.warn("参数错误");
        }
        warehouseProductService.remove(ids.split(","));
        return ResponseUtils.success("删除成功");
    }*/

    /**
     * 导出excel
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/warehouseProduct/exportExcel", method = {RequestMethod.POST, RequestMethod.GET})
    public void exportExcel(HttpServletRequest request, HttpServletResponse response) {
        String warehouseId = ServletRequestUtils.getStringParameter(request, "warehouseId", null);
        String productName = ServletRequestUtils.getStringParameter(request, "productName", null);
        String warnType = ServletRequestUtils.getStringParameter(request, "warnType", null);

        /*if (StringUtils.isNotBlank(name)) {
            try {
                name = new String(name.getBytes("ISO8859-1"), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }*/
        User user = ApiUtils.getLoginUser();

        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        params.put("orgId", user.getOrgId());
        params.put("warehouseId", warehouseId);
        params.put("productName", productName);
        params.put("warnType", warnType);

        List<WarehouseProduct> list = warehouseProductService.queryList(params);
        try {
            response.setContentType("application/vnd.ms-excel");
            response.setCharacterEncoding("utf-8");
            // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
            String fileName = URLEncoder.encode("库存明细_" + DateUtils.getNowTime(), "UTF-8");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");

            // 根据用户传入字段 假设我们要忽略 date
            Set<String> excludeColumnFiledNames = new HashSet<String>();
            excludeColumnFiledNames.add("id");
            excludeColumnFiledNames.add("orgId");
            excludeColumnFiledNames.add("productId");
            excludeColumnFiledNames.add("warehouseId");
            excludeColumnFiledNames.add("shortageLimit");
            excludeColumnFiledNames.add("backlogLimit");
            excludeColumnFiledNames.add("buyPrice");
            excludeColumnFiledNames.add("salePrice");
            excludeColumnFiledNames.add("memberPrice");
            excludeColumnFiledNames.add("tradePrice");
            excludeColumnFiledNames.add("isDel");
            excludeColumnFiledNames.add("exists");
            excludeColumnFiledNames.add("createTime");
            excludeColumnFiledNames.add("modifyTime");
            excludeColumnFiledNames.add("createId");
            excludeColumnFiledNames.add("modifyId");

            // 这里需要设置不关闭流
            EasyExcel.write(response.getOutputStream(), WarehouseProduct.class)
                    .excludeColumnFiledNames(excludeColumnFiledNames)
                    .autoCloseStream(Boolean.FALSE)
                    .sheet("库存明细信息")
                    .doWrite(list);

        } catch (Exception e) {
            e.printStackTrace();
            // 重置response
            response.reset();
            //response.setContentType("application/json");
            //response.setCharacterEncoding("utf-8");
            AjaxUtils.ajaxJsonWarnMessage("下载失败");
        }
    }
}
