package com.yunfeisoft.utils;

import com.applet.utils.DateUtils;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Jackie Liu on 2017/6/28.
 */
public class Test {

    public static void main(String[] args) {
        System.out.println(new BigDecimal("-100").compareTo(BigDecimal.ZERO));
        Date now = new Date();
        Date date = DateUtils.addMoth(now, 1);
        System.out.println(DateUtils.dateToString(date));
    }
}
