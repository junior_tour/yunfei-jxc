<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>编辑备忘录信息</title>
	<#include "/common/vue_resource.ftl">
</head>
<body>
<div id="app" v-cloak>
	<div class="ui-form">
		<form class="layui-form" @submit.prevent="submitForm()" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">标题<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.title" placeholder="请输入标题" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">开始时间<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" id="beginDate" v-model="record.beginDate" placeholder="请输入开始时间" class="layui-input" readonly style="cursor:pointer;"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">结束时间</label>
				<div class="layui-input-block">
					<input type="text" id="endDate" v-model="record.endDate" placeholder="请输入结束时间" class="layui-input" readonly style="cursor:pointer;"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">内容<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<textarea v-model="record.content" placeholder="请输入结束时间" class="layui-textarea"></textarea>
				</div>
			</div>

			<div class="layui-form-item">
				<div class="layui-input-block">
					<input type="submit" value="保存" class="layui-btn" />
				</div>
			</div>
		</form>
	</div>
</div>
<script>
	var app = new Vue({
		el: '#app',
		data: {
			showTypes: false,
			record : {
				id:'${params.id!}',
				title:'',
				beginDate:'${.now?string('yyyy-MM-dd')}',
				endDate:'',
				content:'',
			},
		},
		mounted: function () {
			this.init();
			this.loadData();
		},
		methods: {
			init:function(){
				var that = this;
				laydate.render({elem: '#beginDate', type:'date', done:function (value) {
						that.record.beginDate = value;
					}});

				laydate.render({elem: '#endDate', type:'date', done:function (value) {
						that.record.endDate = value;
					}});
			},
			loadData: function () {
				if (!'${params.id!}') {
					return;
				}
				var that = this;
				$.http.post('${params.contextPath}/web/memorandum/query.json', {id: '${params.id!}'}).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var item = data.data;
					for (var key in  that.record) {
						that.record[key] = item[key];
					}
				});
			},
			submitForm: function () {
				$.http.post('${params.contextPath}/web/memorandum/<#if (params.id)??>modify<#else>save</#if>.json', this.record).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var alt = layer.alert(data.message || "操作成功", function () {
						try {
							parent.app.loadData();
						} catch (e) {
							console.log(e);
						}
						try {
							parent.app.loadMemorandum();
						} catch (e) {
							console.log(e);
						}
						parent.layer.closeAll();
						layer.close(alt);
					});
				});
			},
		}
	});
</script>
</body>

</html>
