DROP TABLE IF EXISTS "tc_attachment";
CREATE TABLE "tc_attachment" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "ref_id_" varchar(50) COLLATE "pg_catalog"."default",
  "original_name_" varchar(100) COLLATE "pg_catalog"."default",
  "path_" varchar(100) COLLATE "pg_catalog"."default",
  "category_" int4,
  "size_" int8,
  "type_" varchar(10) COLLATE "pg_catalog"."default",
  "create_time_" timestamp(6),
  "create_id_" varchar(50) COLLATE "pg_catalog"."default"
)
;

DROP TABLE IF EXISTS "tc_data";
CREATE TABLE "tc_data" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "ref_id_" varchar(50) COLLATE "pg_catalog"."default",
  "content_" varchar(255) COLLATE "pg_catalog"."default",
  "num_" int4
)
;

DROP TABLE IF EXISTS "tr_role_menu";
CREATE TABLE "tr_role_menu" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "menu_id_" varchar(50) COLLATE "pg_catalog"."default",
  "role_id_" varchar(50) COLLATE "pg_catalog"."default"
)
;

DROP TABLE IF EXISTS "tr_role_user";
CREATE TABLE "tr_role_user" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "user_id_" varchar(50) COLLATE "pg_catalog"."default",
  "role_id_" varchar(50) COLLATE "pg_catalog"."default"
)
;

DROP TABLE IF EXISTS "ts_menu";
CREATE TABLE "ts_menu" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "parent_id_" varchar(50) COLLATE "pg_catalog"."default",
  "name_" varchar(50) COLLATE "pg_catalog"."default",
  "order_by_" int4,
  "icon_" varchar(100) COLLATE "pg_catalog"."default",
  "state_" int4,
  "remark_" varchar(200) COLLATE "pg_catalog"."default",
  "category_" int4,
  "url_" varchar(100) COLLATE "pg_catalog"."default",
  "code_" varchar(100) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of ts_menu
-- ----------------------------
INSERT INTO "ts_menu" VALUES ('935706390603788390', '1', '系统管理', 200, 'fa-cog', 1, '', 1, '', 'system');
INSERT INTO "ts_menu" VALUES ('6919226684366447495', '6919226684366447494', '添加商品类别', 201, NULL, 1, '', 2, '', 'productCategory_add');
INSERT INTO "ts_menu" VALUES ('6919226684366447496', '6919226684366447494', '修改商品类别', 202, NULL, 1, '', 2, '', 'productCategory_update');
INSERT INTO "ts_menu" VALUES ('6919226684366447497', '6919226684366447494', '删除商品类别', 203, NULL, 1, '', 2, '', 'productCategory_delete');
INSERT INTO "ts_menu" VALUES ('6919226684366447498', '6919226684366447494', '查看商品类别', 203, NULL, 1, '', 2, '', 'productCategory_detail');
INSERT INTO "ts_menu" VALUES ('6919226684366447500', '6919226684366447499', '添加供应商信息', 201, NULL, 1, '', 2, '', 'supplier_add');
INSERT INTO "ts_menu" VALUES ('6919226684366447501', '6919226684366447499', '修改供应商信息', 202, NULL, 1, '', 2, '', 'supplier_update');
INSERT INTO "ts_menu" VALUES ('6919226684366447502', '6919226684366447499', '删除供应商信息', 203, NULL, 1, '', 2, '', 'supplier_delete');
INSERT INTO "ts_menu" VALUES ('6919226684366447503', '6919226684366447499', '查看供应商信息', 203, NULL, 1, '', 2, '', 'supplier_detail');
INSERT INTO "ts_menu" VALUES ('6919226684366447553', '6919226684366447552', '添加客户信息', 201, NULL, 1, '', 2, '', 'customer_add');
INSERT INTO "ts_menu" VALUES ('6919226684366447554', '6919226684366447552', '修改客户信息', 202, NULL, 1, '', 2, '', 'customer_update');
INSERT INTO "ts_menu" VALUES ('6919226684366447555', '6919226684366447552', '删除客户信息', 203, NULL, 1, '', 2, '', 'customer_delete');
INSERT INTO "ts_menu" VALUES ('6919226684366447556', '6919226684366447552', '查看客户信息', 203, NULL, 1, '', 2, '', 'customer_detail');
INSERT INTO "ts_menu" VALUES ('6919226684366447558', '6919226684366447557', '添加商品信息', 201, NULL, 1, '', 2, '', 'product_add');
INSERT INTO "ts_menu" VALUES ('6919226684366447559', '6919226684366447557', '修改商品信息', 202, NULL, 1, '', 2, '', 'product_update');
INSERT INTO "ts_menu" VALUES ('6919226684366447560', '6919226684366447557', '删除商品信息', 203, NULL, 1, '', 2, '', 'product_delete');
INSERT INTO "ts_menu" VALUES ('6919226684366447561', '6919226684366447557', '查看商品信息', 203, NULL, 1, '', 2, '', 'product_detail');
INSERT INTO "ts_menu" VALUES ('6919226684366447494', '5f18f703b8b1362970fdd251', '商品分类', 10, 'fa-square', 1, '', 1, '/view/business/productCategory/productCategory_list.htm', 'productCategory_list');
INSERT INTO "ts_menu" VALUES ('6919226684366447557', '5f18f703b8b1362970fdd251', '商品资料', 20, 'fa-square', 1, '', 1, '/view/business/product/product_list.htm', 'product_list');
INSERT INTO "ts_menu" VALUES ('6919226684366447499', '5f18f703b8b1362970fdd251', '供应商资料', 30, 'fa-square', 1, '', 1, '/view/business/supplier/supplier_list.htm', 'supplier_list');
INSERT INTO "ts_menu" VALUES ('6919226684366447552', '5f18f703b8b1362970fdd251', '客户资料', 40, 'fa-square', 1, '', 1, '/view/business/customer/customer_list.htm', 'customer_list');
INSERT INTO "ts_menu" VALUES ('6919226684366450692', '5f18f7f7b8b1362970fdd252', '进货单管理', 20, 'fa-square', 1, '', 1, '/view/business/purchaseOrder/purchaseOrder_list.htm', 'purchaseOrder_list');
INSERT INTO "ts_menu" VALUES ('935706390603788392', '935706390603788390', '账号管理', 20, 'fa-user', 1, '', 1, '/view/user/user_list.htm', 'user');
INSERT INTO "ts_menu" VALUES ('6919226684366450693', '6919226684366450692', '添加进货单', 201, NULL, 1, '', 2, '', 'purchaseOrder_add');
INSERT INTO "ts_menu" VALUES ('6919226684366450694', '6919226684366450692', '修改进货单', 202, NULL, 1, '', 2, '', 'purchaseOrder_update');
INSERT INTO "ts_menu" VALUES ('6919226684366450696', '6919226684366450692', '查看进货单', 203, NULL, 1, '', 2, '', 'purchaseOrder_detail');
INSERT INTO "ts_menu" VALUES ('6919226684366450695', '6919226684366450692', '删除进货单', 203, NULL, 1, '', 2, '', 'purchaseOrder_delete');
INSERT INTO "ts_menu" VALUES ('935706390603788395', '935706390603788390', '权限管理', 50, 'fa-users', 1, '', 1, '/view/role/role_list.htm', 'role');
INSERT INTO "ts_menu" VALUES ('5f18f703b8b1362970fdd251', 'ROOT', '基础资料', 10, 'fa-star', 1, '', 1, '', 'basic_data');
INSERT INTO "ts_menu" VALUES ('5f18f7f7b8b1362970fdd252', 'ROOT', '单据管理', 20, 'fa-star', 1, '', 1, '', 'order_manage');
INSERT INTO "ts_menu" VALUES ('5f18f851b8b1362970fdd253', 'ROOT', '财务管理', 30, 'fa-star', 1, '', 1, '', 'financial_manage');
INSERT INTO "ts_menu" VALUES ('6919226684366450702', '5f18f851b8b1362970fdd253', '采购单付款记录', 200, 'fa-square', 1, '', 1, '/view/business/paymentRecord/paymentRecord_list.htm', 'paymentRecord_list');
INSERT INTO "ts_menu" VALUES ('5f18fb0eb8b1362970fdd255', '935706390603788390', '系统参数', 10, 'fa-star', 1, '', 1, '/view/business/sysConfig/sysConfig_edit.htm', 'sysConfig_edit');
INSERT INTO "ts_menu" VALUES ('5f20e6f107906837106d7b7f', '5f18f703b8b1362970fdd251', '公司信息', 200, 'fa-star', 1, '', 1, '/view/organization/company_edit.htm', 'company_edit');
INSERT INTO "ts_menu" VALUES ('6919226684366450755', '5f18f7f7b8b1362970fdd252', '销售单管理', 10, 'fa-square', 1, '', 1, '/view/business/saleOrder/saleOrder_list.htm', 'saleOrder_list');
INSERT INTO "ts_menu" VALUES ('5f21134407906837106d7b88', '5f18f874b8b1362970fdd254', '库存明细', 200, 'fa-star', 1, '', 1, '/view/business/chart/product_stock_alarm.htm', 'product_stock_alarm');
INSERT INTO "ts_menu" VALUES ('6919226684366450765', '5f18f851b8b1362970fdd253', '销售单收款记录', 200, 'fa-square', 1, '', 1, '/view/business/incomeRecord/incomeRecord_list.htm', 'incomeRecord_list');
INSERT INTO "ts_menu" VALUES ('5f2113a407906837106d7b89', '5f18f874b8b1362970fdd254', '利润明细', 200, 'fa-star', 1, '', 1, '/view/business/chart/saleOrder_profit_list.htm', 'saleOrder_profit_list');
INSERT INTO "ts_menu" VALUES ('6919226684366450756', '6919226684366450755', '添加销售单', 201, NULL, 1, '', 2, '', 'saleOrder_add');
INSERT INTO "ts_menu" VALUES ('6919226684366450757', '6919226684366450755', '修改销售单', 202, NULL, 1, '', 2, '', 'saleOrder_update');
INSERT INTO "ts_menu" VALUES ('6919226684366450758', '6919226684366450755', '删除销售单', 203, NULL, 1, '', 2, '', 'saleOrder_delete');
INSERT INTO "ts_menu" VALUES ('6919226684366450759', '6919226684366450755', '查看销售单', 203, NULL, 1, '', 2, '', 'saleOrder_detail');
INSERT INTO "ts_menu" VALUES ('9168189730935532795', '9168189730935532796', '添加收付款信息', 201, NULL, 1, '', 2, '', 'paymentReceived_add');
INSERT INTO "ts_menu" VALUES ('9168189730935532794', '9168189730935532796', '修改收付款信息', 202, NULL, 1, '', 2, '', 'paymentReceived_update');
INSERT INTO "ts_menu" VALUES ('9168189730935532793', '9168189730935532796', '删除收付款信息', 203, NULL, 1, '', 2, '', 'paymentReceived_delete');
INSERT INTO "ts_menu" VALUES ('9168189730935532796', '5f18f851b8b1362970fdd253', '其他收付款管理', 200, 'fa-square', 1, '', 1, '/view/business/paymentReceived/paymentReceived_list.htm', 'paymentReceived_list');
INSERT INTO "ts_menu" VALUES ('5f3385f407906828040e700b', '6919226684366450692', '改单进货单', 200, 'fa-square', 1, '', 2, '', 'change_puarchase_order');
INSERT INTO "ts_menu" VALUES ('5f33858e07906828040e700a', '6919226684366450755', '改单销售单', 200, 'fa-square', 1, '', 2, '', 'change_sale_order');
INSERT INTO "ts_menu" VALUES ('5f338ae707906828040e7010', '6919226684366450755', '收款销售单', 200, '', 1, '', 2, '', 'income_sale_order');
INSERT INTO "ts_menu" VALUES ('5f338b9907906828040e7011', '6919226684366450692', '付款进货单', 200, '', 1, '', 2, '', 'payment_purchase_order');
INSERT INTO "ts_menu" VALUES ('5f312c9107906825c0f89ac5', 'ROOT', '价格显示控制', 200, '', 1, '', 2, '', 'saleOrder_column');
INSERT INTO "ts_menu" VALUES ('5f312cd707906825c0f89ac6', '5f312c9107906825c0f89ac5', '显示销售价及相关金额', 200, '', 1, '', 2, '', 'saleOrder_column_price');
INSERT INTO "ts_menu" VALUES ('5f3c8bb2b8b1361450412ed1', '5f312c9107906825c0f89ac5', '显示进货价及相关金额', 200, '', 1, '', 2, '', 'purchaseOrder_column_price');
INSERT INTO "ts_menu" VALUES ('6918109846875468234', '6918109846875468233', '添加公告信息', 201, NULL, 1, '', 2, '', 'notice_add');
INSERT INTO "ts_menu" VALUES ('6918109846875468235', '6918109846875468233', '修改公告信息', 202, NULL, 1, '', 2, '', 'notice_update');
INSERT INTO "ts_menu" VALUES ('6918109846875468236', '6918109846875468233', '删除公告信息', 203, NULL, 1, '', 2, '', 'notice_delete');
INSERT INTO "ts_menu" VALUES ('6918109846875468237', '6918109846875468233', '查看公告信息', 203, NULL, 1, '', 2, '', 'notice_detail');
INSERT INTO "ts_menu" VALUES ('6918109846875468233', '1', '公告信息', 50, 'fa-star', 1, '', 1, '/view/business/notice/notice_list.htm', 'notice_list');

DROP TABLE IF EXISTS "ts_organization";
CREATE TABLE "ts_organization" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "parent_id_" varchar(50) COLLATE "pg_catalog"."default",
  "id_path_" varchar(512) COLLATE "pg_catalog"."default",
  "code_path_" varchar(512) COLLATE "pg_catalog"."default",
  "code_" varchar(50) COLLATE "pg_catalog"."default",
  "name_" varchar(100) COLLATE "pg_catalog"."default",
  "linkman_id_" varchar(50) COLLATE "pg_catalog"."default",
  "category_" int4,
  "remark_" varchar(200) COLLATE "pg_catalog"."default",
  "telephone_" varchar(20) COLLATE "pg_catalog"."default",
  "create_time_" timestamp(6),
  "area_id_" varchar(50) COLLATE "pg_catalog"."default",
  "account_num_" int4,
  "begin_date_" timestamp(6),
  "end_date_" timestamp(6),
  "alipay_url_" varchar(100) COLLATE "pg_catalog"."default",
  "wxpay_url_" varchar(100) COLLATE "pg_catalog"."default"
)
;

INSERT INTO "ts_organization" VALUES ('5fae825744581a4d9de8a75c', '1', '5fae825744581a4d9de8a75c', NULL, NULL, '公司名称', NULL, 5, NULL, NULL, '2020-11-13 20:55:51.937', NULL, 0, '2020-11-13 20:55:51.937', '2030-11-13 20:55:51.937', NULL, NULL);

DROP TABLE IF EXISTS "ts_position";
CREATE TABLE "ts_position" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "name_" varchar(50) COLLATE "pg_catalog"."default",
  "code_" varchar(50) COLLATE "pg_catalog"."default",
  "remark_" varchar(200) COLLATE "pg_catalog"."default",
  "state_" int4,
  "create_time_" timestamp(6),
  "org_id_" varchar(50) COLLATE "pg_catalog"."default"
)
;

INSERT INTO "ts_position" VALUES ('5f1f9bf0353ce34d3cf04e73', '测试职务', '0001', '', 1, '2020-07-28 11:30:56.707', NULL);

DROP TABLE IF EXISTS "ts_role";
CREATE TABLE "ts_role" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "name_" varchar(50) COLLATE "pg_catalog"."default",
  "state_" int4,
  "create_id_" varchar(50) COLLATE "pg_catalog"."default",
  "create_time_" timestamp(6),
  "is_sys_" int4,
  "remark_" varchar(200) COLLATE "pg_catalog"."default",
  "org_id_" varchar(50) COLLATE "pg_catalog"."default"
)
;

INSERT INTO "ts_role" VALUES ('5fae82d644581a4d9de8a763', '超级管理员', 1, '935706390603788290', '2020-11-13 20:57:58.809', 2, '', '5f190256b8b1362970fdd263');

DROP TABLE IF EXISTS "ts_user";
CREATE TABLE "ts_user" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "name_" varchar(50) COLLATE "pg_catalog"."default",
  "gender_" int4,
  "phone_" varchar(20) COLLATE "pg_catalog"."default",
  "email_" varchar(100) COLLATE "pg_catalog"."default",
  "org_id_" varchar(50) COLLATE "pg_catalog"."default",
  "state_" int4,
  "police_no_" varchar(50) COLLATE "pg_catalog"."default",
  "idcard_" varchar(30) COLLATE "pg_catalog"."default",
  "head_photo_" varchar(200) COLLATE "pg_catalog"."default",
  "telephone_" varchar(20) COLLATE "pg_catalog"."default",
  "create_time_" timestamp(6),
  "is_sys_" int4,
  "pass_" varchar(200) COLLATE "pg_catalog"."default",
  "position_id_" varchar(50) COLLATE "pg_catalog"."default",
  "begin_date_" timestamp(6),
  "end_date_" timestamp(6),
  "open_id_" varchar(100) COLLATE "pg_catalog"."default"
)
;

INSERT INTO "ts_user" VALUES ('5fae825744581a4d9de8a75d', '18812345678', NULL, NULL, NULL, '5fae825744581a4d9de8a75c', 1, '18812345678', NULL, NULL, NULL, '2020-11-13 20:55:51.937', 2, 'F116132F669232C0A67A1F6A9C1CBCFEE4A6D866024B9EB813228FAC', NULL, '2020-11-13 20:55:51.937', '2030-11-13 20:55:51.937', NULL);

DROP TABLE IF EXISTS "tt_code_builder";
CREATE TABLE "tt_code_builder" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "org_id_" varchar(50) COLLATE "pg_catalog"."default",
  "name_" varchar(50) COLLATE "pg_catalog"."default",
  "num_" int4,
  "is_del_" int2,
  "create_id_" varchar(50) COLLATE "pg_catalog"."default",
  "create_time_" timestamp(6),
  "modify_id_" varchar(50) COLLATE "pg_catalog"."default",
  "modify_time_" timestamp(6)
)
;

INSERT INTO "tt_code_builder" VALUES ('5fae825744581a4d9de8a75e', '5fae825744581a4d9de8a75c', 'WAREHOUSE', 1, 2, NULL, '2020-11-13 20:55:51.95', NULL, '2020-11-13 20:55:51.95');

DROP TABLE IF EXISTS "tt_customer";
CREATE TABLE "tt_customer" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "org_id_" varchar(50) COLLATE "pg_catalog"."default",
  "code_" varchar(50) COLLATE "pg_catalog"."default",
  "name_" varchar(50) COLLATE "pg_catalog"."default",
  "linkman_" varchar(50) COLLATE "pg_catalog"."default",
  "type_" int2,
  "fax_" varchar(50) COLLATE "pg_catalog"."default",
  "member_no_" varchar(50) COLLATE "pg_catalog"."default",
  "birth_date_" timestamp(6),
  "phone_" varchar(50) COLLATE "pg_catalog"."default",
  "standby_phone_" varchar(50) COLLATE "pg_catalog"."default",
  "address_" varchar(200) COLLATE "pg_catalog"."default",
  "qq_" varchar(50) COLLATE "pg_catalog"."default",
  "wechat_" varchar(50) COLLATE "pg_catalog"."default",
  "wangwang_" varchar(50) COLLATE "pg_catalog"."default",
  "email_" varchar(50) COLLATE "pg_catalog"."default",
  "remark_" varchar(500) COLLATE "pg_catalog"."default",
  "balance_" numeric(10,2),
  "pinyin_code_" varchar(50) COLLATE "pg_catalog"."default",
  "is_del_" int2,
  "create_id_" varchar(50) COLLATE "pg_catalog"."default",
  "create_time_" timestamp(6),
  "modify_id_" varchar(50) COLLATE "pg_catalog"."default",
  "modify_time_" timestamp(6)
)
;


DROP TABLE IF EXISTS "tt_income_record";
CREATE TABLE "tt_income_record" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "org_id_" varchar(50) COLLATE "pg_catalog"."default",
  "customer_id_" varchar(50) COLLATE "pg_catalog"."default",
  "sale_order_id_" varchar(50) COLLATE "pg_catalog"."default",
  "receive_amount_" numeric(10,2),
  "income_amount_" numeric(10,2),
  "out_amount_" numeric(10,2),
  "remark_" varchar(200) COLLATE "pg_catalog"."default",
  "is_del_" int2,
  "create_id_" varchar(50) COLLATE "pg_catalog"."default",
  "create_time_" timestamp(6),
  "modify_id_" varchar(50) COLLATE "pg_catalog"."default",
  "modify_time_" timestamp(6)
)
;

DROP TABLE IF EXISTS "tt_memorandum";
CREATE TABLE "tt_memorandum" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "org_id_" varchar(50) COLLATE "pg_catalog"."default",
  "title_" varchar(50) COLLATE "pg_catalog"."default",
  "begin_date_" timestamp(6),
  "end_date_" timestamp(6),
  "is_del_" int2,
  "create_id_" varchar(50) COLLATE "pg_catalog"."default",
  "create_time_" timestamp(6),
  "modify_id_" varchar(50) COLLATE "pg_catalog"."default",
  "modify_time_" timestamp(6)
)
;

DROP TABLE IF EXISTS "tt_notice";
CREATE TABLE "tt_notice" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "org_id_" varchar(50) COLLATE "pg_catalog"."default",
  "title_" varchar(50) COLLATE "pg_catalog"."default",
  "is_del_" int2,
  "create_id_" varchar(50) COLLATE "pg_catalog"."default",
  "create_time_" timestamp(6),
  "modify_id_" varchar(50) COLLATE "pg_catalog"."default",
  "modify_time_" timestamp(6),
  "video_url_" varchar(100) COLLATE "pg_catalog"."default",
  "num_" int4,
  "type_" varchar(50) COLLATE "pg_catalog"."default"
)
;

DROP TABLE IF EXISTS "tt_payment_received";
CREATE TABLE "tt_payment_received" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "org_id_" varchar(50) COLLATE "pg_catalog"."default",
  "type_" int2,
  "name_" varchar(50) COLLATE "pg_catalog"."default",
  "amount_" numeric(10,2),
  "remark_" varchar(200) COLLATE "pg_catalog"."default",
  "is_del_" int2,
  "create_id_" varchar(50) COLLATE "pg_catalog"."default",
  "create_time_" timestamp(6),
  "modify_id_" varchar(50) COLLATE "pg_catalog"."default",
  "modify_time_" timestamp(6),
  "occur_date_" timestamp(6)
)
;

DROP TABLE IF EXISTS "tt_payment_record";
CREATE TABLE "tt_payment_record" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "org_id_" varchar(50) COLLATE "pg_catalog"."default",
  "supplier_id_" varchar(50) COLLATE "pg_catalog"."default",
  "purchase_order_id_" varchar(50) COLLATE "pg_catalog"."default",
  "due_amount_" numeric(10,2),
  "pay_amount_" numeric(10,2),
  "owed_amount_" numeric(10,2),
  "remark_" varchar(200) COLLATE "pg_catalog"."default",
  "is_del_" int2,
  "create_id_" varchar(50) COLLATE "pg_catalog"."default",
  "create_time_" timestamp(6),
  "modify_id_" varchar(50) COLLATE "pg_catalog"."default",
  "modify_time_" timestamp(6)
)
;

DROP TABLE IF EXISTS "tt_product";
CREATE TABLE "tt_product" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "org_id_" varchar(50) COLLATE "pg_catalog"."default",
  "code_" varchar(50) COLLATE "pg_catalog"."default",
  "name_" varchar(50) COLLATE "pg_catalog"."default",
  "standard_" varchar(50) COLLATE "pg_catalog"."default",
  "unit_" varchar(50) COLLATE "pg_catalog"."default",
  "category_id_" varchar(50) COLLATE "pg_catalog"."default",
  "supplier_id_" varchar(50) COLLATE "pg_catalog"."default",
  "stock_" numeric(10,2),
  "shortage_limit_" numeric(10,2),
  "backlog_limit_" numeric(10,2),
  "intro_" varchar(500) COLLATE "pg_catalog"."default",
  "buy_price_" numeric(10,2),
  "sale_price_" numeric(10,2),
  "member_price_" numeric(10,2),
  "trade_price_" numeric(10,2),
  "pinyin_code_" varchar(50) COLLATE "pg_catalog"."default",
  "is_del_" int2,
  "create_id_" varchar(50) COLLATE "pg_catalog"."default",
  "create_time_" timestamp(6),
  "modify_id_" varchar(50) COLLATE "pg_catalog"."default",
  "modify_time_" timestamp(6),
  "avg_price_" numeric(10,2)
)
;

DROP TABLE IF EXISTS "tt_product_category";
CREATE TABLE "tt_product_category" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "org_id_" varchar(50) COLLATE "pg_catalog"."default",
  "parent_id_" varchar(50) COLLATE "pg_catalog"."default",
  "id_path_" varchar(500) COLLATE "pg_catalog"."default",
  "code_" varchar(50) COLLATE "pg_catalog"."default",
  "name_" varchar(50) COLLATE "pg_catalog"."default",
  "name_path_" varchar(50) COLLATE "pg_catalog"."default",
  "pinyin_code_" varchar(50) COLLATE "pg_catalog"."default",
  "is_del_" int2,
  "create_id_" varchar(50) COLLATE "pg_catalog"."default",
  "create_time_" timestamp(6),
  "modify_id_" varchar(50) COLLATE "pg_catalog"."default",
  "modify_time_" timestamp(6)
)
;

DROP TABLE IF EXISTS "tt_purchase_item";
CREATE TABLE "tt_purchase_item" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "org_id_" varchar(50) COLLATE "pg_catalog"."default",
  "purchase_order_id_" varchar(50) COLLATE "pg_catalog"."default",
  "product_id_" varchar(50) COLLATE "pg_catalog"."default",
  "quantity_" numeric(10,2),
  "amount_" numeric(10,2),
  "remark_" varchar(200) COLLATE "pg_catalog"."default",
  "is_del_" int2,
  "create_id_" varchar(50) COLLATE "pg_catalog"."default",
  "create_time_" timestamp(6),
  "modify_id_" varchar(50) COLLATE "pg_catalog"."default",
  "modify_time_" timestamp(6),
  "price_" numeric(10,2),
  "warehouse_id_" varchar(50) COLLATE "pg_catalog"."default"
)
;

DROP TABLE IF EXISTS "tt_purchase_order";
CREATE TABLE "tt_purchase_order" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "org_id_" varchar(50) COLLATE "pg_catalog"."default",
  "supplier_id_" varchar(50) COLLATE "pg_catalog"."default",
  "code_" varchar(50) COLLATE "pg_catalog"."default",
  "purchase_date_" timestamp(6),
  "total_amount_" numeric(10,2),
  "remark_" varchar(200) COLLATE "pg_catalog"."default",
  "is_del_" int2,
  "create_id_" varchar(50) COLLATE "pg_catalog"."default",
  "create_time_" timestamp(6),
  "modify_id_" varchar(50) COLLATE "pg_catalog"."default",
  "modify_time_" timestamp(6),
  "status_" int2,
  "pay_status_" int2,
  "pay_amount_" numeric(10,2)
)
;


DROP TABLE IF EXISTS "tt_sale_item";
CREATE TABLE "tt_sale_item" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "org_id_" varchar(50) COLLATE "pg_catalog"."default",
  "sale_order_id_" varchar(50) COLLATE "pg_catalog"."default",
  "product_id_" varchar(50) COLLATE "pg_catalog"."default",
  "quantity_" numeric(10,2),
  "discount_" numeric(10,2),
  "amount_" numeric(10,2),
  "remark_" varchar(200) COLLATE "pg_catalog"."default",
  "is_del_" int2,
  "create_id_" varchar(50) COLLATE "pg_catalog"."default",
  "create_time_" timestamp(6),
  "modify_id_" varchar(50) COLLATE "pg_catalog"."default",
  "modify_time_" timestamp(6),
  "price_" numeric(10,2),
  "sale_price_" numeric(10,2),
  "sale_amount_" numeric(10,2),
  "warehouse_id_" varchar(50) COLLATE "pg_catalog"."default"
)
;

DROP TABLE IF EXISTS "tt_sale_order";
CREATE TABLE "tt_sale_order" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "org_id_" varchar(50) COLLATE "pg_catalog"."default",
  "customer_id_" varchar(50) COLLATE "pg_catalog"."default",
  "code_" varchar(50) COLLATE "pg_catalog"."default",
  "sale_date_" timestamp(6),
  "total_amount_" numeric(10,2),
  "remark_" varchar(200) COLLATE "pg_catalog"."default",
  "is_del_" int2,
  "create_id_" varchar(50) COLLATE "pg_catalog"."default",
  "create_time_" timestamp(6),
  "modify_id_" varchar(50) COLLATE "pg_catalog"."default",
  "modify_time_" timestamp(6),
  "status_" int2,
  "pay_status_" int2,
  "pay_amount_" numeric(10,2),
  "total_cost_amount_" numeric(10,2)
)
;

DROP TABLE IF EXISTS "tt_supplier";
CREATE TABLE "tt_supplier" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "org_id_" varchar(50) COLLATE "pg_catalog"."default",
  "code_" varchar(50) COLLATE "pg_catalog"."default",
  "name_" varchar(50) COLLATE "pg_catalog"."default",
  "linkman_" varchar(50) COLLATE "pg_catalog"."default",
  "fax_" varchar(50) COLLATE "pg_catalog"."default",
  "phone_" varchar(50) COLLATE "pg_catalog"."default",
  "standby_phone_" varchar(50) COLLATE "pg_catalog"."default",
  "address_" varchar(200) COLLATE "pg_catalog"."default",
  "qq_" varchar(50) COLLATE "pg_catalog"."default",
  "wechat_" varchar(50) COLLATE "pg_catalog"."default",
  "wangwang_" varchar(50) COLLATE "pg_catalog"."default",
  "email_" varchar(50) COLLATE "pg_catalog"."default",
  "website_" varchar(200) COLLATE "pg_catalog"."default",
  "bank_account_" varchar(50) COLLATE "pg_catalog"."default",
  "bank_name_" varchar(50) COLLATE "pg_catalog"."default",
  "bank_no_" varchar(50) COLLATE "pg_catalog"."default",
  "remark_" varchar(500) COLLATE "pg_catalog"."default",
  "balance_" numeric(10,2),
  "pinyin_code_" varchar(50) COLLATE "pg_catalog"."default",
  "is_del_" int2,
  "create_id_" varchar(50) COLLATE "pg_catalog"."default",
  "create_time_" timestamp(6),
  "modify_id_" varchar(50) COLLATE "pg_catalog"."default",
  "modify_time_" timestamp(6)
)
;

DROP TABLE IF EXISTS "tt_sys_config";
CREATE TABLE "tt_sys_config" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "key_" varchar(50) COLLATE "pg_catalog"."default",
  "value_" varchar(500) COLLATE "pg_catalog"."default",
  "is_del_" int2,
  "create_id_" varchar(50) COLLATE "pg_catalog"."default",
  "create_time_" timestamp(6),
  "modify_id_" varchar(50) COLLATE "pg_catalog"."default",
  "modify_time_" timestamp(6)
)
;

INSERT INTO "tt_sys_config" VALUES ('5fae834444581a4d9de8a7ba', 'faviconImagePath', NULL, 2, '5fae825744581a4d9de8a75d', '2020-11-13 20:59:48.383', '5fae825744581a4d9de8a75d', '2020-11-13 20:59:48.383');
INSERT INTO "tt_sys_config" VALUES ('5fae834444581a4d9de8a7bb', 'recordNumber', NULL, 2, '5fae825744581a4d9de8a75d', '2020-11-13 20:59:48.383', '5fae825744581a4d9de8a75d', '2020-11-13 20:59:48.383');
INSERT INTO "tt_sys_config" VALUES ('5fae834444581a4d9de8a7bc', 'websitName', NULL, 2, '5fae825744581a4d9de8a75d', '2020-11-13 20:59:48.384', '5fae825744581a4d9de8a75d', '2020-11-13 20:59:48.384');
INSERT INTO "tt_sys_config" VALUES ('5fae834444581a4d9de8a7bd', 'copyright', 'Copyright © 2019-2020 Yunfeisoft.com. 云飞CMS 版权所有', 2, '5fae825744581a4d9de8a75d', '2020-11-13 20:59:48.384', '5fae825744581a4d9de8a75d', '2020-11-13 20:59:48.384');
INSERT INTO "tt_sys_config" VALUES ('5fae834444581a4d9de8a7be', 'logoImagePath', NULL, 2, '5fae825744581a4d9de8a75d', '2020-11-13 20:59:48.384', '5fae825744581a4d9de8a75d', '2020-11-13 20:59:48.384');
INSERT INTO "tt_sys_config" VALUES ('5fae834444581a4d9de8a7bf', 'newAccountValidity', '0', 2, '5fae825744581a4d9de8a75d', '2020-11-13 20:59:48.384', '5fae825744581a4d9de8a75d', '2020-11-13 20:59:48.384');
INSERT INTO "tt_sys_config" VALUES ('5fae834444581a4d9de8a7c0', 'websitShortName', NULL, 2, '5fae825744581a4d9de8a75d', '2020-11-13 20:59:48.384', '5fae825744581a4d9de8a75d', '2020-11-13 20:59:48.384');


DROP TABLE IF EXISTS "tt_warehouse";
CREATE TABLE "tt_warehouse" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "org_id_" varchar(50) COLLATE "pg_catalog"."default",
  "code_" varchar(50) COLLATE "pg_catalog"."default",
  "name_" varchar(50) COLLATE "pg_catalog"."default",
  "is_del_" int2,
  "create_id_" varchar(50) COLLATE "pg_catalog"."default",
  "create_time_" timestamp(6),
  "modify_id_" varchar(50) COLLATE "pg_catalog"."default",
  "modify_time_" timestamp(6),
  "is_default_" int2
)
;

INSERT INTO "tt_warehouse" VALUES ('5fae825744581a4d9de8a75f', '5fae825744581a4d9de8a75c', '01', '存货仓库', 2, NULL, '2020-11-13 20:55:51.983', NULL, '2020-11-13 20:55:51.983', 1);


DROP TABLE IF EXISTS "tt_warehouse_product";
CREATE TABLE "tt_warehouse_product" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "product_id_" varchar(50) COLLATE "pg_catalog"."default",
  "warehouse_id_" varchar(50) COLLATE "pg_catalog"."default",
  "stock_" numeric(10,2),
  "shortage_limit_" numeric(10,2),
  "backlog_limit_" numeric(10,2),
  "is_del_" int2,
  "create_id_" varchar(50) COLLATE "pg_catalog"."default",
  "create_time_" timestamp(6),
  "modify_id_" varchar(50) COLLATE "pg_catalog"."default",
  "modify_time_" timestamp(6),
  "init_stock_" numeric(10,2)
)
;

DROP TABLE IF EXISTS "tt_warehouse_user";
CREATE TABLE "tt_warehouse_user" (
  "id_" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "user_id_" varchar(50) COLLATE "pg_catalog"."default",
  "warehouse_id_" varchar(50) COLLATE "pg_catalog"."default",
  "is_del_" int2,
  "create_id_" varchar(50) COLLATE "pg_catalog"."default",
  "create_time_" timestamp(6),
  "modify_id_" varchar(50) COLLATE "pg_catalog"."default",
  "modify_time_" timestamp(6)
)
;

INSERT INTO "tt_warehouse_user" VALUES ('5fae825744581a4d9de8a760', '5fae825744581a4d9de8a75d', '5fae825744581a4d9de8a75f', 2, NULL, '2020-11-13 20:55:51.987', NULL, '2020-11-13 20:55:51.987');

ALTER TABLE "tc_attachment" ADD CONSTRAINT "tc_attachment_pkey" PRIMARY KEY ("id_");

ALTER TABLE "tc_data" ADD CONSTRAINT "tc_data_pkey" PRIMARY KEY ("id_");

ALTER TABLE "tr_role_menu" ADD CONSTRAINT "tr_role_menu_pkey" PRIMARY KEY ("id_");

ALTER TABLE "tr_role_user" ADD CONSTRAINT "tr_role_user_pkey" PRIMARY KEY ("id_");

ALTER TABLE "ts_menu" ADD CONSTRAINT "ts_menu_pkey" PRIMARY KEY ("id_");

ALTER TABLE "ts_organization" ADD CONSTRAINT "ts_organization_pkey" PRIMARY KEY ("id_");

ALTER TABLE "ts_position" ADD CONSTRAINT "ts_position_pkey" PRIMARY KEY ("id_");

ALTER TABLE "ts_role" ADD CONSTRAINT "ts_role_pkey" PRIMARY KEY ("id_");

ALTER TABLE "ts_user" ADD CONSTRAINT "ts_user_pkey" PRIMARY KEY ("id_");

ALTER TABLE "tt_code_builder" ADD CONSTRAINT "tt_code_builder_pkey" PRIMARY KEY ("id_");

ALTER TABLE "tt_customer" ADD CONSTRAINT "tt_customer_pkey" PRIMARY KEY ("id_");

ALTER TABLE "tt_income_record" ADD CONSTRAINT "tt_income_record_pkey" PRIMARY KEY ("id_");

ALTER TABLE "tt_memorandum" ADD CONSTRAINT "tt_memorandum_pkey" PRIMARY KEY ("id_");

ALTER TABLE "tt_notice" ADD CONSTRAINT "tt_notice_pkey" PRIMARY KEY ("id_");

ALTER TABLE "tt_payment_received" ADD CONSTRAINT "tt_payment_received_pkey" PRIMARY KEY ("id_");

ALTER TABLE "tt_payment_record" ADD CONSTRAINT "tt_payment_record_pkey" PRIMARY KEY ("id_");

ALTER TABLE "tt_product" ADD CONSTRAINT "tt_product_pkey" PRIMARY KEY ("id_");

ALTER TABLE "tt_product_category" ADD CONSTRAINT "tt_product_category_pkey" PRIMARY KEY ("id_");

ALTER TABLE "tt_purchase_item" ADD CONSTRAINT "tt_purchase_item_pkey" PRIMARY KEY ("id_");

ALTER TABLE "tt_purchase_order" ADD CONSTRAINT "tt_purchase_order_pkey" PRIMARY KEY ("id_");

ALTER TABLE "tt_sale_item" ADD CONSTRAINT "tt_sale_item_pkey" PRIMARY KEY ("id_");

ALTER TABLE "tt_sale_order" ADD CONSTRAINT "tt_sale_order_pkey" PRIMARY KEY ("id_");

ALTER TABLE "tt_supplier" ADD CONSTRAINT "tt_supplier_pkey" PRIMARY KEY ("id_");

ALTER TABLE "tt_sys_config" ADD CONSTRAINT "tt_sys_config_pkey" PRIMARY KEY ("id_");

ALTER TABLE "tt_warehouse" ADD CONSTRAINT "tt_warehouse_pkey" PRIMARY KEY ("id_");

ALTER TABLE "tt_warehouse_product" ADD CONSTRAINT "tt_warehouse_product_pkey" PRIMARY KEY ("id_");

ALTER TABLE "tt_warehouse_user" ADD CONSTRAINT "tt_warehouse_user_pkey" PRIMARY KEY ("id_");
